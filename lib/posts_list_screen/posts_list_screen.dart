import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../models/post.dart';
import '../post_detail_screen/post_detail_screen.dart';
import '../post_item/post_item.dart';
import '../posts_bloc/posts_bloc.dart';
import '../repository/posts_repository.dart';

class PostsListScreen extends StatelessWidget {
  const PostsListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PostsBloc(
        repository: RepositoryProvider.of<PostsRepository>(context),
      )..add(GetAllPosts(10)),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Posts'),
              actions: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                )
              ],
            ),
            body: BlocBuilder<PostsBloc, PostsState>(
              builder: (context, state) {
                switch (state.status) {
                  case PostsStatus.initial:
                    return const SizedBox();
                  case PostsStatus.loading:
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  case PostsStatus.error:
                    return Center(
                      child: Text(state.error),
                    );
                  case PostsStatus.success:
                    final posts = state.posts;

                    if(posts.isEmpty) {
                      return const Center(
                        child: Text('Pas de posts disponibles'),
                      );
                    }

                    return ListView.builder(
                      itemCount: posts.length,
                      itemBuilder: (context, index) {
                        final post = posts[index];
                        return PostItem(
                          post: post,
                          onTap: () => _onPostTap(context, post),
                        );
                      },
                    );
                }
              },
            ),
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.refresh),
              onPressed: () => _onRefreshList(context),
            ),
          );
        },
      ),
    );
  }

  void _onRefreshList(BuildContext context) {
    final postsBloc = BlocProvider.of<PostsBloc>(context);
    postsBloc.add(GetAllPosts(10));
  }

  void _onPostTap(BuildContext context, Post post) {
    PostDetailScreen.navigateTo(context, post);
  }
}