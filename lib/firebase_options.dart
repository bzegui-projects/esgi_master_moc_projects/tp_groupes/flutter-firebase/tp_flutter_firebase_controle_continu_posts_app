// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      throw UnsupportedError(
        'DefaultFirebaseOptions have not been configured for web - '
        'you can reconfigure this by running the FlutterFire CLI again.',
      );
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyAfV5N-1M2fc4V8QS_ymPAulPVgqrHx40Y',
    appId: '1:187732465645:android:c55a08fde1540edb8b79e4',
    messagingSenderId: '187732465645',
    projectId: 'tp-flutter-firebase-post-a58dd',
    storageBucket: 'tp-flutter-firebase-post-a58dd.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyB-gWOY2BdP3Av7xCqSzL3dxN5o8IIK9hY',
    appId: '1:187732465645:ios:f7de71fcb753bced8b79e4',
    messagingSenderId: '187732465645',
    projectId: 'tp-flutter-firebase-post-a58dd',
    storageBucket: 'tp-flutter-firebase-post-a58dd.appspot.com',
    iosClientId: '187732465645-n27rofhu25ni5i9jnapc1nk2tnqqap5a.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterMocFirebaseProject',
  );
}
