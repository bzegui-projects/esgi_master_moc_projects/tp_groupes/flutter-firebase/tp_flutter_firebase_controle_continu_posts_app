import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_moc_firebase_project/data_sources/posts_data_sources.dart';

import '../models/post.dart';

class ApiPostsDataSource extends PostsDataSource {

  final List<Post> _posts = [];
  final CollectionReference postsCollectionReference = FirebaseFirestore.instance.collection('posts');

  @override
  Future<List<Post>> getPostsCollection() async {
    _posts.clear();
    QuerySnapshot snapshot = await postsCollectionReference.get();
     List<dynamic> data = snapshot.docs.map((e) => e.data()).toList();

     for (var postData in data) {
        Post post = Post.fromJson(postData);
        _posts.add(post);
     }
     return _posts;
  }
}