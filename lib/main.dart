import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_moc_firebase_project/analytics/analytics_provider.dart';
import 'package:flutter_moc_firebase_project/post_detail_screen/post_detail_screen.dart';
import 'package:flutter_moc_firebase_project/posts_list_screen/posts_list_screen.dart';
import 'package:flutter_moc_firebase_project/repository/posts_repository.dart';
import 'analytics/firebase_analytics_handler.dart';
import 'data_sources/api_posts_data_sources.dart';
import 'firebase_options.dart';
import 'models/post.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(name: 'tp-flutter-firebase-posts-app', options: DefaultFirebaseOptions.currentPlatform);

  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };

  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };

  const apiBaseUrl = String.fromEnvironment('API_BASE_URL', defaultValue: '/posts-list');
  debugPrint('API_BASE_URL: $apiBaseUrl');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => PostsRepository(
        remotePostsDataSource: ApiPostsDataSource(),
      ),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          textTheme: const TextTheme(
            bodySmall: TextStyle(
              fontSize: 20,
              color: Colors.orange,
            ),
          ),
        ),
        routes: {
          '/': (context) => const PostsListScreen()
        },
        onGenerateRoute: (settings) {
          Widget content = const SizedBox.shrink();

          switch (settings.name) {
            case PostDetailScreen.routeName:
              final arguments = settings.arguments;
              if (arguments is Post) {
                content = PostDetailScreen(post: arguments);
              }
              break;
          }

          return MaterialPageRoute(
            builder: (context) {
              return content;
            },
          );
        },

      ),
    );
  }
}






