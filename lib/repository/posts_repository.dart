import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import '../data_sources/posts_data_sources.dart';
import '../models/post.dart';

class PostsRepository {
  final PostsDataSource remotePostsDataSource;

  PostsRepository({
    required this.remotePostsDataSource,
  });

  Future<List<Post>> getPosts() async {
    try {
      final posts = await remotePostsDataSource.getPostsCollection();
      return posts;
    } catch (error) {
      FirebaseCrashlytics.instance.recordError(error, StackTrace.current);
      rethrow;
    }
  }
}